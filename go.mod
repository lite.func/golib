module lib

go 1.21.5

require (
	github.com/google/go-cmp v0.6.0
	github.com/lib/pq v1.10.9
)
