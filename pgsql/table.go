package pgsql

type Table[T any] interface {
	TableName() string
	Columns() []string
	ColumnValues(cols ...string) ([]interface{}, error)
	ColumnPtrs(t *T, cols ...string) ([]interface{}, error)
}

func selectColumns(opts1, opts2 []string) []string {

	if len(opts1) != 0 {
		return opts1
	} else {
		return opts2
	}
}
