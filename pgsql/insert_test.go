package pgsql

import (
	"lib/pgsql/testdata"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestInsert(t *testing.T) {
	db, err := testdata.Connect(testdata.DbCfg.String())
	if err != nil {
		t.Error(err)
		return
	}

	defer deleteAll(t, db, testdata.TbB{}.TableName())

	if _, err := Insert(db, testdata.TbB1); err != nil {
		t.Error(err)
		return
	}

	if _, err := Insert(db, testdata.TbB2, "int1", "text1"); err != nil {
		t.Error(err)
		return
	}

	mds := selects[testdata.TbB](t, db, nil)

	md2 := testdata.TbB2
	md2.Bool1 = false
	if !cmp.Equal([]testdata.TbB{testdata.TbB1, md2}, mds) {
		t.Errorf(`want %+v, got %+v`, []testdata.TbB{testdata.TbB1, md2}, mds)
		return
	}
}
