package pgsql

import (
	"lib/pgsql/query"
	"testing"
)

func deleteAll(t *testing.T, db DB, tb string) {
	q := query.New().DeleteFrom(tb)
	if _, err := db.Exec(q.SQL()); err != nil {
		t.Fatal(t)
	}
}

func selects[TB Table[T], T any](t *testing.T, db DB, where map[string]interface{}, columns ...string) []T {

	mds, err := Select[TB, T](db, where, columns...)
	if err != nil {
		t.Fatal(t)
	}
	return mds
}
