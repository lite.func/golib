package testdata

type TbA struct {
	Id    int64  `json:"id"`
	Bool1 bool   `json:"bool1"`
	Int1  int    `json:"int1"`
	Text1 string `json:"text1"`
}

func (re TbA) TableName() string {
	return "test.tb_a"
}

func (re TbA) Columns() []string {
	return []string{"id", "bool1", "int1", "text1"}
}

func (re TbA) ColumnPtrs(cols ...string) (*TbA, []interface{}, error) {
	var md TbA

	ptrs := make([]interface{}, len(cols))
	for i, col := range cols {
		var ptr interface{}
		switch col {
		case "id":
			ptr = &md.Id
		case "bool1":
			ptr = &md.Bool1
		case "int1":
			ptr = &md.Int1
		case "text1":
			ptr = &md.Text1
		default:
			return &md, ptrs, errKeyNotFound
		}
		ptrs[i] = ptr
	}
	return &md, ptrs, nil
}

func (re TbA) ColumnValues(cols ...string) ([]interface{}, error) {

	vs := make([]interface{}, len(cols))
	for i, col := range cols {
		var v interface{}
		switch col {
		case "id":
			v = re.Id
		case "bool1":
			v = re.Bool1
		case "int1":
			v = re.Int1
		case "text1":
			v = re.Text1
		default:
			return vs, errKeyNotFound
		}
		vs[i] = v
	}
	return vs, nil
}
