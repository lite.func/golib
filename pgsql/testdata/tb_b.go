package testdata

var (
	TbB1 = TbB{
		Bool1: true,
		Int1:  1,
		Text1: "a",
	}
	TbB2 = TbB{
		Bool1: true,
		Int1:  2,
		Text1: "b",
	}
	TbBs = []TbB{TbB1, TbB2}
)

type TbB struct {
	Bool1 bool   `json:"bool1"`
	Int1  int    `json:"int1"`
	Text1 string `json:"text1"`
}

func (re TbB) TableName() string {
	return "test.tb_b"
}

func (re TbB) Columns() []string {
	return []string{"bool1", "int1", "text1"}
}

func (re TbB) ColumnPtrs(md *TbB, cols ...string) ([]interface{}, error) {

	ptrs := make([]interface{}, len(cols))
	for i, col := range cols {
		var ptr interface{}
		switch col {
		case "bool1":
			ptr = &md.Bool1
		case "int1":
			ptr = &md.Int1
		case "text1":
			ptr = &md.Text1
		default:
			return ptrs, errKeyNotFound
		}
		ptrs[i] = ptr
	}
	return ptrs, nil
}

func (re TbB) ColumnValues(cols ...string) ([]interface{}, error) {

	vs := make([]interface{}, len(cols))
	for i, col := range cols {
		var v interface{}
		switch col {
		case "bool1":
			v = re.Bool1
		case "int1":
			v = re.Int1
		case "text1":
			v = re.Text1
		default:
			return vs, errKeyNotFound
		}
		vs[i] = v
	}
	return vs, nil
}
