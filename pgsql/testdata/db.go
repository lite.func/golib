package testdata

import (
	"database/sql"
	"fmt"
	"lib/logger"

	_ "github.com/lib/pq"
)

type DataSource struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Database string `json:"database"`
	User     string `json:"user"`
	Password string `json:"password"`
	SSL      string `json:"ssl"`
}

func (re *DataSource) String() string {
	return fmt.Sprintf("port=%d host=%s user=%s password=%s dbname=%s sslmode=%s",
		re.Port, re.Host, re.User, re.Password, re.Database, re.SSL)
}

// Connect represents connecting to databse
func Connect(dbConfig string) (*sql.DB, error) {

	logger.Debug(dbConfig)

	var err error
	db, err := sql.Open("postgres", dbConfig)
	if err != nil {
		logger.Error("db connection", err)
		return nil, err
	}

	return db, nil
}

var DbCfg = DataSource{
	Host:     "localhost",
	Port:     5431,
	Database: "test",
	User:     "test",
	Password: "1234qwer",
	SSL:      "disable",
}
