package testdata

import "errors"

var errKeyNotFound = errors.New("key not found")

// type Tb struct {
// 	Id     int64     `json:"id"`
// 	Bool1  int       `json:"bool1"`
// 	Int1   string    `json:"int1"`
// 	Text1  string    `json:"text1"`
// 	Time1  time.Time `json:"time1"`
// 	Timez1 time.Time `json:"timez1"`
// 	Texts  []string  `json:"texts"`
// 	Ints   []int     `json:"ints"`
// }

// func (re Tb) TableName() string {
// 	return "test.tb"
// }

// func (re Tb) Columns() []string {
// 	return []string{"id", "bool1", "int1", "text1", "time1", "timez1", "texts", "ints"}
// }

// func (re Tb) Scan() (*Tb, map[string]interface{}) {
// 	var md Tb

// 	return &md, map[string]interface{}{
// 		"id":     &md.Id,
// 		"bool1":  &md.Bool1,
// 		"int1":   &md.Int1,
// 		"text1":  &md.Text1,
// 		"time1":  &md.Time1,
// 		"timez1": &md.Timez1,
// 		"texts":  &md.Texts,
// 		"ints":   &md.Ints,
// 	}
// }
