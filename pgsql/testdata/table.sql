CREATE SCHEMA test;

CREATE TABLE IF NOT EXISTS test.tb (
  id SERIAL PRIMARY KEY,
  bool1 boolean,
  int1 Integer,
  text1 TEXT,
  time1 timestamp,
  timez1 timestamp with time zone,
  ints Integer [],
  texts TEXT []
);


CREATE TABLE IF NOT EXISTS test.tb_a (
  id SERIAL PRIMARY KEY,
  bool1 boolean,
  int1 Integer,
  text1 TEXT
);

CREATE TABLE IF NOT EXISTS test.tb_b (
  bool1 boolean default false,
  int1 Integer default 0,
  text1 TEXT default ''
);

CREATE TABLE IF NOT EXISTS test.tb_array (i Integer [], t TEXT []);

CREATE TABLE IF NOT EXISTS test.tb_time (
  time timestamp,
  timez timestamp with time zone,
  value jsonb
);