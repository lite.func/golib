package query

import (
	"fmt"
	"strings"
)

func (q Query) InsertInto(tb string, cols ...string) Query {
	q.sqls = append(q.sqls, "INSERT INTO", tb)
	if len(cols) != 0 {
		q.sqls = append(q.sqls, fmt.Sprintf(`(%s)`, strings.Join(cols, ", ")))
	}
	return q
}

func (q Query) InsertIntoValues(tb string, cols ...string) Query {
	// s := strings.Join(Placeholders(1, len(cols)), ", ")
	// s1 := fmt.Sprintf(`VALUES(%s)`, s)
	// q.sqls = append(q.sqls, "INSERT INTO", s1)
	return q.InsertInto(tb, cols...).Values(Placeholders(1, len(cols)))
}

func (q Query) Values(colss ...[]string) Query {

	ss := make([]string, len(colss))
	for i, cols := range colss {
		ss[i] = fmt.Sprintf(`(%s)`, strings.Join(cols, ", "))
	}
	q.sqls = append(q.sqls, "VALUES", strings.Join(ss, ", "))
	return q
}

func (q Query) Returning(cols ...string) Query {
	q.sqls = append(q.sqls, "RETURNING", strings.Join(cols, ", "))
	return q
}
