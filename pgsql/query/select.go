package query

import (
	"fmt"
	"strings"

	_ "github.com/lib/pq"
)

func (q Query) Select(cols ...string) Query {
	q.sqls = append(q.sqls, "SELECT", strings.Join(cols, ", "))
	return q
}

func (q Query) From(s string) Query {
	q.sqls = append(q.sqls, "FROM", s)
	return q
}

func (q Query) Where(condition string) Query {
	q.sqls = append(q.sqls, "WHERE", condition)
	return q
}

func (q Query) As(s string) Query {
	q.sqls = append(q.sqls, "AS", s)
	return q
}

func (q Query) GroupBy(cols ...string) Query {
	q.sqls = append(q.sqls, "GROUP BY", strings.Join(cols, ", "))
	return q
}

func (q Query) Having(condition string) Query {
	q.sqls = append(q.sqls, "HAVING", condition)
	return q
}

func (q Query) OrderBy(cols ...string) Query {
	q.sqls = append(q.sqls, "ORDER BY", strings.Join(cols, ", "))
	return q
}

func (q Query) Limit(n uint64) Query {
	q.sqls = append(q.sqls, fmt.Sprintf("LIMIT %d", n))
	return q
}

func (q Query) Offset(n uint64) Query {
	q.sqls = append(q.sqls, fmt.Sprintf("OFFSET %d", n))
	return q
}
