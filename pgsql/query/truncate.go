package query

func (q Query) Truncate(tb string) Query {
	q.sqls = append(q.sqls, "TRUNCATE", tb)
	return q
}
