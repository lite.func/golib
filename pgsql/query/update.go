package query

func (q Query) Update(tb string) Query {
	q.sqls = append(q.sqls, "UPDATE", tb)
	return q
}

func (q Query) Set(s string) Query {
	q.sqls = append(q.sqls, "SET", s)
	return q
}
