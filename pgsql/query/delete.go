package query

func (q Query) DeleteFrom(tb string) Query {
	q.sqls = append(q.sqls, "DELETE FROM", tb)
	return q
}
