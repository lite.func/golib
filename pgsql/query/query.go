package query

import (
	"fmt"
	"strings"
)

type Query struct {
	sqls []string
}

func New() Query {
	return Query{}
}

func (q Query) SQL() string {
	return strings.Join(q.sqls, " ") + ";"
}

func (q Query) S(ss ...string) Query {
	q.sqls = append(q.sqls, ss...)
	return q
}

func Placeholders(start, length int) []string {
	ss := make([]string, length)
	for i := 0; i < length; i++ {
		ss[i] = fmt.Sprintf(`$%d`, start+i)
	}
	return ss
}

func ArgsSet(start int, cols ...string) []string {
	args := make([]string, len(cols))
	for i, col := range cols {
		args[i] = fmt.Sprintf(`%s=$%d`, col, start)
		start++
	}
	return args
}

func ArgSet(start int, cols ...string) string {
	return strings.Join(ArgsSet(start, cols...), ", ")
}

func ArgAnd(start int, cols ...string) string {
	return strings.Join(ArgsSet(start, cols...), " AND ")
}

func ArgOr(start int, cols ...string) string {
	return strings.Join(ArgsSet(start, cols...), " OR ")
}

func Distinct(cols ...string) string {
	s := "DISTINCT"
	if len(cols) != 0 {
		s = s + " " + strings.Join(cols, ", ")
	}
	return s
}

func Count(cols ...string) string {
	return fmt.Sprintf(`COUNT(%s)`, strings.Join(cols, ", "))
}
