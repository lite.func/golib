package pgsql

import (
	"database/sql"
	"lib/pgsql/query"
)

func Insert[T any](db DB, tb Table[T], columns ...string) (sql.Result, error) {

	cols := selectColumns(columns, tb.Columns())

	vs, err := tb.ColumnValues(cols...)
	if err != nil {
		return nil, err
	}

	q := query.New().InsertIntoValues(tb.TableName(), cols...)
	return db.Exec(q.SQL(), vs...)
}
