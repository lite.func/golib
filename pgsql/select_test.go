package pgsql

import (
	"lib/pgsql/testdata"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSelect(t *testing.T) {
	db, err := testdata.Connect(testdata.DbCfg.String())
	if err != nil {
		t.Error(err)
		return
	}

	defer deleteAll(t, db, testdata.TbB{}.TableName())

	if _, err := Insert(db, testdata.TbB1); err != nil {
		t.Error(err)
		return
	}

	if _, err := Insert(db, testdata.TbB2); err != nil {
		t.Error(err)
		return
	}

	md, err := SelectOne[testdata.TbB](db, nil)
	if err != nil {
		t.Error(err)
		return
	}

	if testdata.TbB1 != md {
		t.Errorf(`want %+v, got %+v`, testdata.TbB1, md)
		return
	}

	md, err = SelectOne[testdata.TbB](db, nil, "int1", "text1")
	if err != nil {
		t.Error(err)
		return
	}

	md1 := testdata.TbB1
	md1.Bool1 = false
	if md1 != md {
		t.Errorf(`want %+v, got %+v`, md1, md)
		return
	}

	md, err = SelectOne[testdata.TbB](db, map[string]interface{}{"int1": 2, "text1": "b"})
	if err != nil {
		t.Error(err)
		return
	}
	if testdata.TbB2 != md {
		t.Errorf(`want %+v, got %+v`, testdata.TbB2, md)
		return
	}

	mds, err := Select[testdata.TbB](db, nil)
	if err != nil {
		t.Error(err)
		return
	}

	if !cmp.Equal(testdata.TbBs, mds) {
		t.Errorf(`want %+v, got %+v`, testdata.TbBs, mds)
		return
	}

	mds, err = Select[testdata.TbB](db, nil, "int1", "text1")
	if err != nil {
		t.Error(err)
		return
	}

	md2 := testdata.TbB2
	md2.Bool1 = false
	if !cmp.Equal([]testdata.TbB{md1, md2}, mds) {
		t.Errorf(`want %+v, got %+v`, []testdata.TbB{md1, md2}, mds)
		return
	}

	mds, err = Select[testdata.TbB](db, map[string]interface{}{"int1": 2, "text1": "b"})
	if err != nil {
		t.Error(err)
		return
	}

	if !cmp.Equal([]testdata.TbB{testdata.TbB2}, mds) {
		t.Errorf(`want %+v, got %+v`, []testdata.TbB{testdata.TbB2}, mds)
		return
	}
}
