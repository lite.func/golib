package pgsql

import (
	"database/sql"
	"lib/pgsql/query"
)

func Update[T any](db DB, tb Table[T], set_cols []string, where_cols ...string) (sql.Result, error) {

	vs, err := tb.ColumnValues(set_cols...)
	if err != nil {
		return nil, err
	}

	vs1, err := tb.ColumnValues(where_cols...)
	if err != nil {
		return nil, err
	}

	q := query.New().Update(tb.TableName()).Set(query.ArgSet(1, set_cols...)).Where(query.ArgAnd(len(set_cols)+1, where_cols...))

	return db.Exec(q.SQL(), append(vs, vs1...)...)
}
