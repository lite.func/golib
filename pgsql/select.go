package pgsql

import (
	"fmt"
	"lib/pgsql/query"
	"strings"
)

func SelectOne[TB Table[T], T any](db DB, where map[string]interface{}, columns ...string) (T, error) {

	var tb TB
	cols := selectColumns(columns, tb.Columns())
	var md T
	dest, err := tb.ColumnPtrs(&md, cols...)
	if err != nil {
		return md, err
	}

	q := query.New().Select(cols...).From(tb.TableName())
	args := []interface{}{}
	if where != nil {
		ss := []string{}
		n := 1
		for k, v := range where {
			ss = append(ss, fmt.Sprintf(`%s=$%d`, k, n))
			n++
			args = append(args, v)
		}
		q = q.Where(strings.Join(ss, " AND "))
	}
	err = db.QueryRow(q.SQL(), args...).Scan(dest...)
	return md, err
}

func Select[TB Table[T], T any](db DB, where map[string]interface{}, columns ...string) ([]T, error) {

	var tb TB
	cols := selectColumns(columns, tb.Columns())

	q := query.New().Select(cols...).From(tb.TableName())
	args := []interface{}{}
	if where != nil {
		ss := []string{}
		n := 1
		for k, v := range where {
			ss = append(ss, fmt.Sprintf(`%s=$%d`, k, n))
			n++
			args = append(args, v)
		}
		q = q.Where(strings.Join(ss, " AND "))
	}

	mds := []T{}
	rows, err := db.Query(q.SQL(), args...)
	if err != nil {
		return mds, err
	}

	for rows.Next() {
		var md T
		dest, err := tb.ColumnPtrs(&md, cols...)
		if err != nil {
			return mds, err
		}
		if err := rows.Scan(dest...); err != nil {
			return mds, err
		}
		mds = append(mds, md)
	}
	return mds, err
}
