package pgsql

import (
	"database/sql/driver"
	"errors"
)

const null = "null"

type JSONB []byte

func (j JSONB) IsNull() bool {
	return len(j) == 0 || string(j) == null
}

func (j JSONB) Value() (driver.Value, error) {
	if j.IsNull() {

		return nil, nil
	}
	return string(j), nil
}

func (j *JSONB) Scan(value interface{}) error {

	if value == nil {
		*j = nil
		return nil
	}
	s, ok := value.([]byte)
	if !ok {
		return errors.New("Scan source is not bytes")
	}

	*j = append([]byte{}, s...)

	return nil
}

// MarshalJSON returns m as the JSON encoding of m.
func (m JSONB) MarshalJSON() ([]byte, error) {
	if m == nil {
		return []byte(null), nil
	}
	return m, nil
}

// UnmarshalJSON sets *m to a copy of data.
func (m *JSONB) UnmarshalJSON(data []byte) error {
	if m == nil {
		return errors.New("json.RawMessage: UnmarshalJSON on nil pointer")
	}
	if string(data) == null {
		*m = nil
		return nil
	}
	*m = append((*m)[0:0], data...)
	return nil
}
