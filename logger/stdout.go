package logger

import (
	"fmt"
	"os"
	"runtime"
	"time"
)

type StdoutLogger struct {
	*config
	color bool
}

var stdout = &StdoutLogger{
	config: &config{
		flag:          LstdFlag,
		level:         LstdLevel,
		backtrace:     LError | LPanic | LFatal,
		fileFormatter: TrimWd,
	},
	color: true,
}

func GetStdoutLogger() *StdoutLogger {
	return stdout
}

func (re *StdoutLogger) SetColor(color bool) *StdoutLogger {
	re.color = color
	return re
}

func (re *StdoutLogger) Log(t time.Time, frames runtime.Frames, level Level, v ...interface{}) {

	if !re.level.contains(level) {
		return
	}

	s := re.genLog(t, frames, level, v...)
	if re.color {
		level.color().println(s)
	} else {
		fmt.Fprintln(os.Stdout, s)
	}
}
