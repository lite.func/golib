package logger

import (
	"fmt"
	"runtime"
	"strings"
	"time"
)

type Flag int

const (
	Ltime Flag = 1 << iota
	Llevel
	Lfile
	Lline
	LstdFlag = Ltime | Llevel | Lfile | Lline
)

type config struct {
	flag          Flag
	level         Level
	backtrace     Level
	fileFormatter func(string) string
}

func (re *config) SetFlag(flag Flag) *config {
	re.flag = flag
	return re
}

func (re *config) SetLevel(level Level) *config {
	re.level = level
	return re
}

func (re *config) SetBacktrace(level Level) *config {
	re.backtrace = level
	return re
}

func (re *config) SetFileFormatter(ff func(string) string) *config {
	re.fileFormatter = ff
	return re
}

func (re *config) UpdateConfig(flag Flag, level, backtrace Level, ff func(string) string) *config {
	re.SetFlag(flag)
	re.SetLevel(level)
	re.SetBacktrace(backtrace)
	re.SetFileFormatter(ff)
	return re
}

func genMsg(msg ...interface{}) string {
	var msgs []string
	for _, v := range msg {
		msgs = append(msgs, fmt.Sprintf("%+v", v))
	}
	return strings.Join(msgs, " ")
}

func (re *config) genLog(t time.Time, frames runtime.Frames, level Level, v ...interface{}) string {

	fs := []string{}

	var file string
	var line int

	i := 0
	for {
		frame, more := frames.Next()

		i++
		if i <= 4 {
			continue
		}

		// fmt.Println(i, more, frame.Function, frame.File, frame.Line)

		f := re.fileFormatter(frame.File)
		if i == 5 {
			file = f
			line = frame.Line
			if re.backtrace&level == 0 {
				break
			}
		}

		s := fmt.Sprintf("\n%s\n\t", frame.Function)

		if re.flag&Lfile != 0 {
			s += f
		}

		if re.flag&Lline != 0 {
			s += fmt.Sprintf(":%d", frame.Line)
		}

		fs = append(fs, s)

		// it can end up in infinite loop without !more condition
		if frame.Function == "main.main" || !more {
			break
		}

	}

	s := ""
	if re.flag&Ltime != 0 {
		s += fmt.Sprintf("%s ", t.Format("2006-01-02 15:04:05"))
	}
	if re.flag&Llevel != 0 {
		s += fmt.Sprintf("%s ", level.stringIndent())
	}
	if re.flag&Lfile != 0 {
		s += fmt.Sprintf("%s:", file)
	}
	if re.flag&Lline != 0 {
		s += fmt.Sprintf("%d:", line)
	}

	s += fmt.Sprintf(" %s", genMsg(v...))

	if re.backtrace&level != 0 {
		begin := "----- backtrace begin -----"
		trace := strings.Join(fs, "")
		end := "----- backtrace end -----"
		s += fmt.Sprintf("\n%s%s\n%s", begin, trace, end)
	}

	return s
}
