package logger

import (
	"fmt"
	"os"
	"runtime"
	"time"
)

type Logger interface {
	Log(t time.Time, frames runtime.Frames, level Level, v ...interface{})
}

type Loggers struct {
	loggers []Logger
}

var loggers = &Loggers{
	loggers: []Logger{defaultLogger},
}

func GetLoggers() *Loggers {
	return loggers
}

func (re *Loggers) AddLoggers(loggers ...Logger) *Loggers {
	re.loggers = append(re.loggers, loggers...)
	return re
}

func (re *Loggers) SetLoggers(loggers ...Logger) *Loggers {
	re.loggers = loggers
	return re
}

func CallersFrames() *runtime.Frames {
	pc := make([]uintptr, 10)
	n := runtime.Callers(0, pc)
	if n == 0 {
		return nil
	}

	pc = pc[:n] // pass only valid pcs to runtime.CallersFrames
	return runtime.CallersFrames(pc)
}

func (re *Loggers) log(level Level, msg ...interface{}) {
	t := time.Now().UTC()
	frames := CallersFrames()
	for _, v := range re.loggers {
		v.Log(t, *frames, level, msg...)
	}
}

func (re *Loggers) logf(level Level, format string, msg ...interface{}) {
	t := time.Now().UTC()
	frames := CallersFrames()
	for _, v := range re.loggers {
		v.Log(t, *frames, level, fmt.Sprintf(format, msg...))
	}
}

func Trace(msg ...interface{}) {
	loggers.log(LTrace, msg...)
}

func Debug(msg ...interface{}) {
	loggers.log(LDebug, msg...)
}

func Info(msg ...interface{}) {
	loggers.log(LInfo, msg...)
}

func Warn(msg ...interface{}) {
	loggers.log(LWarn, msg...)
}

func Error(msg ...interface{}) {
	loggers.log(LError, msg...)
}

func Panic(msg ...interface{}) {
	loggers.log(LPanic, msg...)
	panic(msg)
}

func Fatal(msg ...interface{}) {
	loggers.log(LFatal, msg...)
	os.Exit(1)
}

func Tracef(format string, msg ...interface{}) {
	loggers.logf(LTrace, format, msg...)
}

func Debugf(format string, msg ...interface{}) {
	loggers.logf(LDebug, format, msg...)
}

func Infof(format string, msg ...interface{}) {
	loggers.logf(LInfo, format, msg...)
}

func Warnf(format string, msg ...interface{}) {
	loggers.logf(LWarn, format, msg...)
}

func Errorf(format string, msg ...interface{}) {
	loggers.logf(LError, format, msg...)
}

func Panicf(format string, msg ...interface{}) {
	loggers.logf(LPanic, format, msg...)
	panic(msg)
}

func Fatalf(format string, msg ...interface{}) {
	loggers.logf(LFatal, format, msg...)
	os.Exit(1)
}
