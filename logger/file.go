package logger

import (
	"os"
	"runtime"
	"time"
)

type FileLogger struct {
	*config
	f *os.File
}

var file = &FileLogger{
	config: &config{
		flag:          LstdFlag,
		level:         LstdLevel,
		backtrace:     LError | LPanic | LFatal,
		fileFormatter: TrimWd,
	},
}

func GetFileLogger() *FileLogger {
	return file
}

func (re *FileLogger) SetFile(f *os.File) *FileLogger {
	re.f = f
	return re
}

func (re *FileLogger) Log(t time.Time, frames runtime.Frames, level Level, v ...interface{}) {

	if !re.level.contains(level) || re.f == nil {
		return
	}

	s := re.genLog(t, frames, level, v...)
	re.f.WriteString(s + "\n")
}
