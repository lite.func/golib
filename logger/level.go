package logger

import (
	"fmt"
	"strings"
)

type Level int

const (
	LTrace Level = 1 << iota
	LDebug
	LInfo
	LWarn
	LError
	LPanic
	LFatal
	LstdLevel = LTrace | LDebug | LInfo | LWarn | LError | LPanic | LFatal
)

func (rec Level) string() string {
	switch rec {
	case LTrace:
		return "Trace"

	case LDebug:
		return "Debug"

	case LInfo:
		return "Info"

	case LWarn:
		return "Warn"

	case LError:
		return "Error"

	case LPanic:
		return "Panic"

	case LFatal:
		return "Fatal"

	default:
		return ""
	}
}

func (re Level) color() *color {
	switch re {
	case LTrace:
		return newColor(FgBrBlue)

	case LDebug:
		return newColor(FgBrCyan)

	case LInfo:
		return newColor(FgBrGreen)

	case LWarn:
		return newColor(FgBrYellow)

	case LError:
		return newColor(FgBrRed)

	case LPanic:
		return newColor(FgBrMagenta)

	case LFatal:
		return newColor(FgRed, BgBrBlue)

	default:
		return newColor(FgBrWhite)
	}
}

func (rec Level) contains(level Level) bool {
	return rec&level != 0
}

func (rec Level) stringIndent() string {
	n := 5 - len(rec.string())
	if n < 0 {
		n = 0
	}
	return fmt.Sprintf("[%s]%s", rec.string(), strings.Repeat(" ", n))
}
