package logger

import (
	"os"
	"runtime"
	"time"
)

type LevelWriter interface {
	Write(level Level, p []byte) (n int, err error)
}

type DefaultLogger struct {
	*config
	writers []LevelWriter
}

type ColorWriter struct {
}

func (re *ColorWriter) Write(level Level, p []byte) (n int, err error) {
	return level.color().write(p)
}

var defaultLogger = &DefaultLogger{
	config: &config{
		flag:          LstdFlag,
		level:         LstdLevel,
		backtrace:     LError | LPanic | LFatal,
		fileFormatter: TrimWd,
	},
	writers: []LevelWriter{new(ColorWriter)},
}

func GetDefaultLogger() *DefaultLogger {
	return defaultLogger
}

func (re *DefaultLogger) AddWriters(writers ...LevelWriter) *DefaultLogger {
	re.writers = append(re.writers, writers...)
	return re
}

func (re *DefaultLogger) SetWriters(writers ...LevelWriter) *DefaultLogger {
	re.writers = writers
	return re
}

func (re *DefaultLogger) Log(t time.Time, frames runtime.Frames, level Level, v ...interface{}) {

	if !re.level.contains(level) {
		return
	}

	s := re.genLog(t, frames, level, v...) + "\n"

	p := []byte(s)
	for _, w := range re.writers {
		w.Write(level, p)
	}
}

type FileWriter struct {
	f *os.File
}

func NewFileWrter(f *os.File) *FileWriter {
	return &FileWriter{
		f: f,
	}
}

func (re *FileWriter) Write(level Level, p []byte) (n int, err error) {
	return re.f.Write(p)
}
