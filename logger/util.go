package logger

import (
	"log"
	"os"
	"strings"
)

func TrimWd(s string) string {
	pwd, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	return strings.TrimPrefix(s, pwd+"/")
}

func GenTrimPrefixFunc(prefix string) func(string) string {

	f := func(s string) string {
		return strings.TrimPrefix(s, prefix)
	}
	return f
}

func TrimToMaxSize(filename string, max_size int64) error {

	fi, err := os.Stat(filename)
	if err != nil {
		Error(err)
		return err
	}

	if fi.Size() > max_size {
		by, err := os.ReadFile(filename)
		if err != nil {
			Error(err)
			return err
		}
		if len(by) > int(max_size) {

			by = by[len(by)-int(max_size):]
			if err := os.WriteFile(filename, by, 0644); err != nil {
				Error(err)
				return err
			}
		}
	}

	return nil
}
